---
layout: page
title: About Me
modified: 2016-03-27
comments: false
image: 
permalink: /about/index.html
tags: []
---

Hi there! I'm Sebastian Cachia, though most people just call me Seb. I was born and raised in the European island nation of [Malta](https://maricacachia.wordpress.com/2016/03/16/malta-my-home-country/). I currently live in Budapest with my wife [Marica](http://maricacachia.wordpress.com) and our dog Flora.

<figure>
  <img src="../images/posts/about_family.jpg">
</figure>


Professionally, I am a technology geek with a short attention span and a passion for learning. So far I have found Product Management to be the best fit, though I am also interested in Engineering Management and User Experience Design. I sometimes hack around in code, though mostly to serve my own needs (nothing production grade here).

I love being outdoors! I grew up (with my now wife) in Scouts and enjoy hiking, running, camping, or anything else I can find time for.  I have been hiking in the Bavarian Alps, Snowdonia and the Black Mountains in Wales and climbed Kilimanjaro in Tanzania. I also enjoy cooking, reading, strong coffee and travel.

<figure>
  <img src="../images/posts/about_safari.jpg">
</figure>

This blog is mostly intended as a repository for posts tackling my professional interests, though I might occasionally deviate into any of my other current (or future) interests.

### Getting in Touch
If you know me professionally, please connect to me on [LinkedIn](http://linkedin.com/in/sebcachia), if you know me personally, add me on [Facebook](http://facebook.com/sebcachia). If we have never met, please connect on [Twitter](http://twitter.com/sebcachia) or send me a [short](http://five.sentenc.es/) email on mail@thisdomain.com.
